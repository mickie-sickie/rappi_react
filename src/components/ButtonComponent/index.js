import React from 'react';
import PropTypes  from 'prop-types';

class ButtonComponent  extends React.Component { 
    render(){
        const label = this.props.label
        return (
            <button onClick={this.props.clicked}>{label}</button>
        )
    }
}
ButtonComponent.propTypes = { 
    label: PropTypes.string,
    clicked: PropTypes.func
}
ButtonComponent.defaultProps = {
    label : '',
    clicked: ()=>console.log('no function')
}
export default ButtonComponent;