import React from 'react';
import PropTypes  from 'prop-types';
import _ from "lodash";
class HeaderComponent  extends React.Component {
    
    constructor(props) {
        super(props)
            this.handleDelete = this.handleDelete.bind(this);
      }
    state = {
        condition:  false,
        storage :   JSON.parse(localStorage.getItem('myItems'))
      }
    handleClick = ()=>{
        this.setState({
        condition: !this.state.condition
        });
  }
  handleDelete = (key)=>{
    let storageItems =JSON.parse(localStorage.getItem('myItems')); 
    let filtered = storageItems.filter(item => item.id  !== key.id);
     localStorage.setItem('myItems' ,JSON.stringify(filtered)  )
     this.setState({
        storage:filtered
     })
  }
  handleAdd =(item)=>{
    let storageItems =JSON.parse(localStorage.getItem('myItems')); 
    storageItems.push(item);
    localStorage.setItem('myItems' ,JSON.stringify(storageItems)  )
    this.setState({
        storage:storageItems
     })
  }
    render(){
       const catego = this.props.categories;
       const storageItems = JSON.parse(localStorage.getItem('myItems'))
        return (
            <header className="App-header">
               <nav className="nav-menu">
                { catego.categories.map((item,index) => 
                    <div key={index} className="nav-item">
                        <a className="first-lvl"> { item.name}</a>
                         { item.sublevels  ? 
                            <div className="subItem-wrapper"> 
                                  {item.sublevels.map((subtem,index)=> 
                                    <div className="subitem item second-level" key={index}>
                                        <span>{subtem.name} </span> 
                                        {subtem.sublevels ? 
                                            <div className="third-lvl">
                                            {subtem.sublevels.map((thirdItem,index)=>
                                                <span className="third-item" key={index}>{thirdItem.name}</span>)}
                                            </div>: ''} 
                                    </div> 
                                )}        
                            </div> : '  ' }                      
                    </div> )}
               </nav>
               <div  className="shopping-car">
                    <span className="icon-shop"  onClick={ this.handleClick } ></span>
                 <div className={ this.state.condition ? "tooltip-car  is-visible" : "tooltip-car"}>
                    {storageItems  ? 
                        <div className="storage-wrappers">
                        {storageItems.map((storageItem,index)=> 
                            <div key={index} className="wrapper-item-storage">
                                <h5>Producto :{storageItem.name}</h5>
                                <p>Precio: {storageItem.price}</p>
                                <p>Disponibiliad:{ storageItem.available  === true  ? 'Yes' : 'No'} </p>
                                <button className="btn  delete" onClick={()=>this. handleDelete(storageItem)}>Eliminar Producto</button>
                                <button className="btn "   onClick={()=>this. handleAdd(storageItem)}>Agregar otro</button>
                            </div>
                            )}
                        </div> : ''
                    }
                    </div>
               </div>
			</header>
        )
    }
}
HeaderComponent.propTypes = { 
    categories: PropTypes.object
}
HeaderComponent.defaultProps = {
    categories : []
}
export default HeaderComponent;