import React from 'react';
import PropTypes  from 'prop-types';

class CardComponent  extends React.Component { 
  constructor(props) {
    super(props)
        this.saveInCar = this.saveInCar.bind(this);
  }
  state = {
    storage :   JSON.parse(localStorage.getItem('myItems'))
  }
  saveInCar = ()=>{
    let  myItems;
    myItems = JSON.parse(localStorage.getItem('myItems')); 
    myItems = myItems === null  ? myItems = [] : myItems;
     myItems.push(this.props)
    localStorage.setItem('myItems' ,JSON.stringify(myItems)  )
    
  }  
  render(){
    
       const  {
        quantity,
        price,
        available,
        name,
        id,
       } = this.props
       
       
        return (
          
          <div className="card-container" key={id}>
            <span className="name">name: {name}</span>
            <span className="price">Price: {price}</span>
            <span className="name">available: {available === true ? 'Yes' : 'No'}</span>
            <span className="name">name: {name}</span>
            <span  className="name">In stock : {quantity}</span>
            <button onClick={this.saveInCar}>Guardar en carrito</button>
          </div>
        )
    }
}
CardComponent.propTypes = { 
    quantity: PropTypes.number,
    price: PropTypes.string,
    available: PropTypes.bool,
    name: PropTypes.string,
    id: PropTypes.string
}
CardComponent.defaultProps = {
    quantity: '',
    price: '',
    available: false,
    name: '',
    id:''
}
export default CardComponent;