import React from 'react';
import './App.css';
import _ from "lodash";
import  HeaderComponent from './components/HeaderComponent';
import  ButtonComponent from './components/ButtonComponent';
import  CardComponent from './components/CardComponent'
import categories from './mockups/categories.json';
import products from './mockups/products.json';

export default class App extends React.Component{
	constructor() {
		super()
		this.state = {
		products: []
		}
	  }
	  componentWillMount() {
		this.setState({
		categories:categories,
		products:products.products,
		productsFiltered: products.products

		})
	  }

	
	byAvailable = ()=>{ 
		let flag = false;
		flag = !flag;
		const filtered = this.state.products.filter(item=> item.available === flag);
		this.setState({
			productsFiltered : filtered
		})
	}
	
	prices =(event) =>{	
		const x = this.state.products.map((item)=> item);
		const flag = event.target.value;
		const  filtered = x.filter(item =>   Number(item.price.replace("$","").replace(",","")) >= Number(flag)  )
		this.setState({
			productsFiltered : filtered
		})
	}
	inStock= (event)=> {
		const x = this.state.products.map((item)=> item);
		const flag = event.target.value;
		const  filtered = x.filter(item =>   item.quantity >= Number(flag)  )
		this.setState({
			productsFiltered : filtered
		})
	}
	HandleChangePrice = (event)=> {
		let x = this.state.products.map((item)=> item); 
		let upDown = event.target.value ;
		x =  x.map((item)=>{ item.rex_price =   Number(item.price.replace("$","").replace(",","")) ; return  item} ) 
		 const ordered =	 _.orderBy( x,	['rex_price'], (upDown === 'up' ? ['desc'] :  upDown === 'down' ? ['asc'] : '' ))
		 this.setState({
			productsFiltered : ordered
		})
	}
	handleStock = (event)=> { 
		let upDown = event.target.value;
		let itemsProds = this.state.products.map((item)=> item)
		const ordered = _.orderBy(itemsProds, ['quantity'] ,(upDown === 'up' ? ['desc'] :  upDown === 'down' ? ['asc'] : '' ))
		this.setState({
			productsFiltered : ordered
		})
	}
	handleAvailable = (event)=>{
		let trueFalse = event.target.value;
		console.log('trueFa',trueFalse)
		let x = this.state.products.map((item)=> item); 
		let  ordened = _.sortBy(x, ['available',  false]);
		if(trueFalse === 'true'){
			ordened = _.reverse(ordened)
			this.setState({
				productsFiltered : ordened
			})
		}
		if(trueFalse === 'false'){
			this.setState({
				productsFiltered : ordened
			})
		}
	}

	render() {
		const  infoButton = [
		{label:'disponibilidad' , clicked: this.byAvailable},
	]
	const renderButtons = infoButton.map((item,index)=> <ButtonComponent key={index} {...item}/>)
		return (
			<div className="App">
			<HeaderComponent  categories={this.state.categories} />
			<div className="main-container">
				<div className="order-wrapper">
					<h4>Ordenar por precio</h4>
					<select onChange={this.HandleChangePrice}>
						<option value={null}>Select</option>
						<option value="up">Mayor precio</option>
						<option value="down">Menor precio</option>
					</select>
					<h4>Ordenar por disponibilidad</h4>
					<select  onChange={this.handleAvailable}>
						<option value={null}>Select</option>
						<option  value='true' >Disponibles</option>
						<option value='false'>No disponibles</option>
					</select>
					<h4>Ordenar por cantidad</h4>
					<select onChange={this.handleStock}>
						<option value={null}>Select</option>
						<option  value="up">Mayor cantidad</option>
						<option value="down">Menor cantidad</option>
					</select>
				</div>
			</div>
			<main className="main-container  not-padding">
				<aside>
					<h2>FILTRAR POR:</h2>
					<span> Disponibilidad: </span>
						{renderButtons}
					<span>Rango de precios:</span>
					<input type="number" onChange={this.prices.bind(this)}></input>
					<span>Cantidad en Stock: </span>
						<input type="stock" onChange={this.inStock.bind(this)}></input>
				</aside>

				<div  className="wrapper-products ">
				  {	this.state.productsFiltered.map((item,index)=> 
					<CardComponent  key={index}  {...item} />
					)} 
				</div>
			</main>
	
			</div>
		);
	}

}

